/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.robotproject;

/**
 *
 * @author Acer
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(10,20,88,88,100);
        System.out.println(robot);
        robot.walk('N');
        System.out.println(robot);
        robot.walk('S');
        robot.walk('S');
        System.out.println(robot);
        robot.walk('E', 2);
        System.out.println(robot);
        robot.walk();
        System.out.println(robot);
        robot.walk(3);
        System.out.println(robot);
        robot.walk('E',72);
        System.out.println(robot);
        robot.walk('S',67);
        System.out.println(robot);
        robot.walk('N',105);
        System.out.println(robot);
    }
}
